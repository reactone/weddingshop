import autoprefixer from "autoprefixer";

export default {
  plugins: [
    autoprefixer({
      browsers: ["last 3 versions", "IE >= 9", "safari >= 8"]
    })
  ]
};
