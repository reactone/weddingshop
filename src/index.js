import "@styles/styles.scss";

import React from "react";
import ReactDOM from "react-dom";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import cancel from "@icons/cancel.svg";

class App extends React.Component {
  constructor(props, context) {
    super(props);
  }

  state = {
    store: [
      {
        title: "Travel Book",
        count: 0,
        price: 12.49,
        taxrate: 1.0,
        imgurl:
          "https://lonelyplanet-weblinc.netdna-ssl.com/product_images/lonely_planet_us/the-travel-book-3/pdp/59f279d0f92ea14f2f8264b4/pdp_main.jpg?c=1509063120"
      },
      {
        title: "Music CD",
        count: 0,
        price: 14.99,
        taxrate: 1.2,
        imgurl:
          "https://cdn.pixabay.com/photo/2012/04/15/18/57/dvd-34919_960_720.png"
      },
      {
        title: "Chocolate Bar",
        count: 0,
        price: 0.85,
        taxrate: 1.0,
        imgurl:
          "https://live.staticflickr.com/7801/47492168651_e9ec71c0c0_b.jpg"
      },
      {
        title: "Imported chocolate",
        count: 0,
        price: 10,
        taxrate: 1.05,
        imgurl:
          "https://live.staticflickr.com/7841/33615563158_834a605a3b_b.jpg"
      },
      {
        title: "Imported perfume",
        count: 0,
        price: 47.5,
        taxrate: 1.25,
        imgurl:
          "https://www.publicdomainpictures.net/pictures/50000/nahled/perfume.jpg"
      },
      {
        title: "Bottle of Perfume",
        count: 0,
        price: 18.99,
        taxrate: 1.2,
        imgurl:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSco7jcDDZi6vh-5NRyqqlDJwrwRUO1AUmcfYbTrnjjNDzA8V8Z"
      },
      {
        title: "Packet of painkillers",
        count: 0,
        price: 9.75,
        taxrate: 1.0,
        imgurl:
          "https://c.pxhere.com/photos/4c/d5/medications_cure_tablets_pharmacy_medical_the_disease_get_sick_treat_yourself-968279.jpg!d"
      }
    ],

    products: []
  };

  componentWillMount() {
    this.initialState = this.state;
  }

  //Function handles quantity change

  onChange = (index, val) => {
    this.setState({
      products: this.state.products.map((product, i) =>
        i === index ? { ...product, count: val } : product
      )
    });
  };

  //Cart reset function

  onReset = () => {
    this.setState(this.initialState);
  };

  // Display Products

  displayProducts() {
    return this.state.store.map((item, index) => {
      return (
        <div key={index} className="productGrid">
          <Card className="productcard">
            <img className="productimage" src={item.imgurl} alt="new" />

            <CardContent>
              <h4>{item.title}</h4>
              <h4>${item.price}</h4>
            </CardContent>
            <CardActions>
              <Button
                variant="contained"
                color="secondary"
                size="small"
                onClick={() => {
                  {
                    this.setState({
                      products: this.state.products.concat({
                        title: item.title,
                        count: 1,
                        price: item.price,
                        taxrate: item.taxrate
                      })
                    });
                  }
                }}
              >
                Add to Cart
              </Button>{" "}
            </CardActions>
          </Card>
        </div>
      );
    });
  }

  //Show notify message

  notify = () => toast("Thank you for your order.");

  render() {
    return (
      <div>
        <div>
          <ToastContainer />
        </div>

        {this.displayProducts()}

        <Card className="card">
          <CardContent>
            <ProductList
              products={this.state.products}
              onChange={this.onChange}
              onReset={this.onReset}
            />
          </CardContent>
          <CardActions className="cardfooter">
            <Total products={this.state.products} onReset={this.onReset} />
            <div className="checkout">
              <Button variant="contained" color="primary" onClick={this.notify}>
                Checkout
              </Button>
            </div>
          </CardActions>
        </Card>
      </div>
    );
  }
}

//Row styles

const textStyle = {
  fontSize: "14px",
  color: "#58565f"
};

const priceStyle = {
  fontSize: "14px",
  color: "#e8a37e",
  fontWeight: "bold"
};

const iconStyle = {
  margin: "10px"
};

const ProductList = ({ products, onChange }) => (
  <div>
    {products.map((product, i) => (
      <table key={i}>
        <tbody>
          <tr>
            <td className="firstcell">
              <span style={textStyle}>{product.title}</span>
            </td>
            <td>
              {" "}
              <input
                type="text"
                value={product.count}
                onChange={e => onChange(i, parseInt(e.target.value) || 0)}
              />
            </td>
            <td className="pricecell">
              <span style={priceStyle}>
                ${(product.price * product.count).toFixed(2)}
              </span>
            </td>
            <td className="lastcell">
              <img
                style={iconStyle}
                width="8"
                src={cancel}
                onClick={() => {
                  {
                    onChange(i, 0);
                  }
                }}
              />
            </td>
          </tr>
        </tbody>
      </table>
    ))}
  </div>
);

const Total = ({ products, onReset }) => (
  <div>
    <h3 className="totalprice">
      Net: $
      {products.reduce((sum, i) => (sum += i.count * i.price), 0).toFixed(2)}
    </h3>

    <h3 className="totalprice">
      Total: $
      {products
        .reduce((sum2, i) => (sum2 += i.count * i.price * i.taxrate), 0)
        .toFixed(2)}
    </h3>

    <Button
      className="clearbutton"
      onClick={() => {
        {
          onReset();
        }
      }}
    >
      Clear
    </Button>
  </div>
);

ReactDOM.render(<App />, document.getElementById("root"));
